"""Design a binary tree and then write an algorithm to print the least(nearest) two common parent(if 2 parents exist otherwise 1  common parent) node between 2 nodes of a binary tree for given 2 key values 
which are present in binary tree"""

class Node:
    # A utility function to create a new node 
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data
         
    def findPath(self,visited,value):
        visited.append(self.data)
        node_visited = False
        if self.data == value:
            return visited
        if self.left != None:
            path = self.left.findPath(visited,value)
            if path != None:
                return path
        if self.right != None:
            path = self.right.findPath(visited,value)
            if path != None:
                return path
        visited.pop()    
        
root = Node(2)
root.left = Node(1)
root.right = Node(3)
root.right.left = Node(4)
root.right.right = Node(5)
root.right.right.right = Node(6)

# Find Path  and get Input 

path1 = []
path2 = []
path1 = root.findPath(path1,4) 
path2 = root.findPath(path2,6)
if path1 == None or path2 == None:
    print('No common parent')
else:
    min = len(path1) if len(path2) > len(path1) else len(path2)
    common_parent = []
    for i in range(0,min-1):
        if path1[i] == path2[i]:
            common_parent.append(path1[i])
        else:
            break
    if len(common_parent) == 1:
        print common_parent[0]
    elif len(common_parent) == 2:
        print str(common_parent[-2]) + " and " + str(common_parent[-1])