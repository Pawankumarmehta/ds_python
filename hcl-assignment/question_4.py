"""A string of lowercase characters in range ascii['a..'z']. We want to reduce the string to its shortest length by doing a series of operations. In each operation we select a pair of adjacent lowercase letters that match, and delete them. For instance, the string aab could be shortened to b in one operation. Now we have to delete as many characters as possible using this method and print the resulting string. If the final string is empty, print Empty String

Function Description

Complete the MaxReducedString function. It should return the super reduced string or Empty String if the final string is empty.

superReducedString has the following parameter(s)"""

def superReducedString(s):
    stack = [] # stack
    for x in s:
        if stack and stack[-1] == x:
            stack.pop()
        else:
            stack.append(x)    
    stack = ''.join(stack)
    return stack or 'Empty String'

if __name__ == "__main__":
	print(superReducedString('aaabccddd'))