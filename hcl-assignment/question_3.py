"""Design a  binary tree and then print the breadth first order traversal of this tree. In breadth first order traversal, we visit the nodes of same height first then go to nodes of next height or level."""

class Node: 

    # A utility function to create a new node 
    def __init__(self, key): 
        self.data = key  
        self.left = None
        self.right = None


def levelOrder(root):
    if root is None: return
    tree = [root]
    while(len(tree) > 0):  #till node is null 
        node = tree.pop(0)
        print node.data,
        if node.left: tree.append(node.left)  # get left Node
        if node.right: tree.append(node.right) # get rigt Node

root = Node(1)
root.right = Node(2)
root.right.right = Node(5)
root.right.right.left = Node(3)
root.right.right.right = Node(6)
root.right.right.left.right = Node(4)

if __name__ == "__main__":
    print "The level order traversal is -"
    levelOrder(root)