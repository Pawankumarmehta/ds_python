"""  Design a system (Python/Java) and write the  code  which can download hundreds  of csv files and parse and put them in Database(You can choose mySQL or MongoDB etc). You have been given only one Machine with Linux ( with Multi-core CPU – 8 vCPU)  installed. File should be downloaded from remote server at some IP using SFTP.  And remote server allows maximum only one SFTP connection at a time from any remote machine(One IP only One SFTP  connection allowed).

The format of data  in CSV File is given below. And like below there will be thousands of different files. Cell Id and Timestamp would be unique for each row.  Each file name is also unique as illustrated below."""

import pysftp
import pandas as pd
import pyodbc


#SFTP  Connection Configuration  
Hostname = "serverdomain or ip.com"
Username = "root"
Password = "12345"



def get_files(Hostname,Username,Password):
	all_files = []
	with pysftp.Connection(host=Hostname, username=Username, password=Password) as sftp:
		print "Connection succesfully stablished ... "

		#reach this path
		sftp.cwd('/filepath')

		# Obtain structure of the remote directory 
		directory_structure = sftp.listdir_attr()

		# Print data
		for attr in directory_structure:
			print attr.filename, attr
			if 'Perf-1-' in attr.filename:   #check starting csv 
				all_files.append(attr.filename)
				sftp.get('/filepath/%s'%attr.filename, './%s'%attr.filename)  # Download csv file on home dir
		return all_files

all_files_name = get_files(Hostname,Username,Password) # Get all File 

# Import CSV

for file in all_files_name:

	data = pd.read_csv ('/home/pathname/%s'%file)
	df = pd.DataFrame(data, columns= ['ResultTime','GranularityPeriod','ObjectName',' CellID','CallAttemps'])

	# Connect to SQL Server
	conn = pyodbc.connect('Driver={SQL Server};'
	                      'Server=RON\SQLEXPRESS;'
	                      'Database=TestDB;'
	                      'Trusted_Connection=yes;')
	cursor = conn.cursor()

	# Create Table
	cursor.execute('CREATE TABLE data_info (ResultTime nvarchar(50),GranularityPeriod nvarchar(50), ObjectName nvarchar(50), CellID int,CallAttemps int)')

	# Insert DataFrame to Table
	for row in df.itertuples():
	    cursor.execute('''
	                INSERT INTO TestDB.dbo.data_info (ResultTime, GranularityPeriod, ObjectName,CellID,,CallAttemps)
	                VALUES (?,?,?,?,?)
	                ''',
	                row.ResultTime, 
	                row.GranularityPeriod,
	                row.ObjectName,
	                row.CellID,
	                row.CallAttemps
	                )
	conn.commit()

